JSON.clean = (input) => JSON.parse(JSON.stringify(input));
JSON.purge = (input) => JSON.parse(JSON.stringify(input).replace(/"\w+":(null|""|\[]|{}),?/g, '').replace(',}', '}'));
