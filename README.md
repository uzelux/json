# JSON Plus
An extension to the build-in JSON object
___

## USAGE

```javascript
require('@uzelux/json');
```

### Clean

This is to remove all undefined values within the JSON
```javascript
JSON.clean({a: 'value', b: undefined, c: null, d: '', e: [], f: {}});
// {a: 'value', c: null, d: '', e: [], f: {}}
``` 

### Purge
This is to remove all empty values (null, empty string, empty array, zero property object)
```javascript
JSON.clean({a: 'value', b: undefined, c: null, d: '', e: [], f: {}});
// {a: 'value'}
``` 

